<?php
class ArtWork {
    public $id;
    public $title;
    public $description;
    public $year;
    public $width;
    public $height;
    public $genre;
    public $gallery;
    public $price;
    public $url;
    
    function __construct($newID,$newTitle,$newDesc,$newYear,$newWidth,$newHeight,
    $newGenre,$newGallery,$newPrice,$newUrl){
        $this->id = $newID;
        $this->title = $newTitle;
        $this->description = $newDesc;
        $this->year = $newYear;
        $this->width = $newWidth;
        $this->height = $newHeight;
        $this->genre = $newGenre;
        $this->gallery = $newGallery;
        $this->price = $newPrice;
        $this->url = $newUrl;
    }
    
    function printArtWork($firstLine){
        if($firstLine)
            echo "<div class='item active'>";
        else
            echo "<div class='item'>";
            
        echo "<div class='container'>
        <div class='col-md-10'>
         <h2>" . $this->title . "</h2>
         <p><a href='#'>" . $this->year . "</a></p>
         <div class='row'>
            <div class='col-md-5'><img class='img-thumbnail img-responsive' src='Resources/art-images/paintings/medium/" . $this->id . ".jpg' alt='" . $this->title . "' title='" . $this->title . "'/></div>
            <div class='col-md-7'>
               <p style='text-align: justify; 	text-justify: inter-word;'>" . $this->description . "</p>
               <p class='price'>" . $this->price . "</p>
               <div class='btn-group btn-group-lg'>
                 <button class='btn btn-default' type='button'>
                     <a href='#'><span class='glyphicon glyphicon-gift'></span> Add to Wish List</a>  
                 </button>
                 <button class='btn btn-default' type='button'>
                  <a href='#'><span class='glyphicon glyphicon-shopping-cart'></span> Add to Shopping Cart</a>
                 </button>
               </div>               
               <p>&nbsp;</p>
               <div class='panel panel-default'>
                 <div class='panel-heading'>Product Details</div>
                 <table class='table'>
                   <tbody><tr>
                     <th>Date:</th>
                     <td>" . $this->year . "</td>
                   </tr>
                   <tr>
                     <th>Medium:</th>
                     <td>" . $this->genre . "</td>
                   </tr>  
                   <tr>
                     <th>Dimensions:</th>
                     <td>" . $this->height . "cm x " . $this->width . "cm</td>
                   </tr> 
                   <tr>
                     <th>Home:</th>
                     <td><a href='#'>" . $this->gallery . "</a></td>
                   </tr>  
                   <tr>
                     <th>Link:</th>
                     <td><a href='" . $this->url . "'>Wiki</a></td>
                   </tr>     
                 </tbody></table>
               </div>                              
               
            </div>  <!-- end col-md-7 -->
         </div>  <!-- end row (product info) -->
          </div>
          </div>
          </div>";
    }
    
    function printArtJumbotron($firstPic){
        if($firstPic)
            echo "<div class='item active'>";
        else
            echo "<div class='item'>";
        echo "<img src='Resources/art-images/paintings/medium/" . $this->id . ".jpg' alt='" . $this->title . "' title='" . $this->title . "' rel='#PaintingThumb' /><div class='container'><div class='carousel-caption'><h1>" . $this->title . "</h1><p>" . $this->year . "</p><p><a class='btn btn-lg btn-primary' href='" . $this->url . "' role='button'>Learn more</a></p></div></div></div>";
    }
    
    function printRowPic(){
        echo "<div class='col-lg-4'><img class='img-circle' src='Resources/art-images/paintings/medium/" . $this->id . ".jpg' alt='" . $this->title . "' title='" . $this->title . "' style='width:100px; height:100px;' /><h2>" . $this->title . "</h2><p class='text-justify'>" . explode(".",$this->description)[0] . ".</p><p><a class='btn btn-default' href='" . $this->url . "' role='button'>View details &raquo;</a></p></div>";
    }
}

?>