<?php
class ArtistCollection {
    public $collection = array();
    
    function __construct() {
        $artistFile = fopen("Resources/data-files/artists.txt","r");
        while(!feof($artistFile)){
            $line = explode("~",fgets($artistFile));
            $this->collection[] = new artistObj($line[0],$line[1],$line[2],$line[3],$line[4],$line[5],$line[6],$line[7]);
        }
        fclose($artistFile);
    }
}

?>