<?php
class ArtCollection{
    public $collection = array();
    
    function __construct(){
        $artFile = fopen("Resources/data-files/paintings.txt","r");
        while(!feof($artFile)){
            $line = explode("~",fgets($artFile));
            $this->collection[] = new ArtWork($line[3],$line[4],$line[5],
                $line[6],$line[7],$line[8],$line[9],$line[10],$line[11],
                    $line[12]);
        }
        fclose($artFile);
    }
}


?>