<?php
include "ArtCollection.php";
include "ArtWork.php"
?>

<!DOCTYPE html>
<html lang="en">
  <head>
   
   <meta http-equiv="Content-Type" content="text/html;charset=us-ansi">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Lab3 SE3316A</title>
    
    <link href='https://fonts.googleapis.com/css?family=Cuprum|Cookie' rel='stylesheet' type='text/css'>  

    <!-- Bootstrap core CSS -->
    <link href="Resources/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="carousel.css" rel="stylesheet">
    <link href="lab3.css" rel="stylesheet">
  </head>
  
  <!-- //////////////////////////////////////////////////////////////////////      -->
  
   <body>
  
  
  
  
  
  <!-- //////////////////////////////////////////////////////////////////////      -->
  
  
  
  
  <header>

   <div id="topHeaderRow">
      <div class="container">
         <nav role="navigation" class="navbar navbar-inverse ">
            <div class="navbar-header">
               <button data-target=".navbar-ex1-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
               </button>
               <p class="navbar-text">Welcome to <strong>Art Store</strong>, <a class="navbar-link" href="#">Login</a> or <a class="navbar-link" href="#">Create new account</a></p>
            </div>

            <div class="collapse navbar-collapse navbar-ex1-collapse pull-right">
               <ul class="nav navbar-nav">
                  <li><a href="#"><span class="glyphicon glyphicon-user"></span> My Account</a></li>
                  <li><a href="#"><span class="glyphicon glyphicon-gift"></span> Wish List</a></li>
                  <li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</a></li>
                  <li><a href="#"><span class="glyphicon glyphicon-arrow-right"></span> Checkout</a></li>                  
               </ul>
            </div>  <!-- end .collpase --> 
         </nav>  <!-- end .navbar --> 
      </div>  <!-- end .container --> 
   </div>  <!-- end #topHeaderRow --> 
   
   <div id="logoRow">
      <div class="container">
         <div class="row">
            <div class="col-md-8">
                <h1>Art Store</h1> 
            </div>
            
            <div class="col-md-4">
               <form role="search" class="form-inline">
                  <div class="input-group">
                     <label for="search" class="sr-only">Search</label>
                     <input type="text" name="search" placeholder="Search" class="form-control">
                     <span class="input-group-btn">
                     <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                     </span>
                  </div>
               </form> 
            </div>   <!-- end .navbar --> 
         </div>   <!-- end .row -->        
      </div>  <!-- end .container --> 
   </div>  <!-- end #logoRow --> 
   
   <div id="mainNavigationRow">
      <div class="container">

         <nav role="navigation" class="navbar navbar-default">
            <div class="navbar-header">
               <button data-target=".navbar-ex1-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
               </button>
            </div>

            <div class="collapse navbar-collapse navbar-ex1-collapse">
             <ul class="nav navbar-nav">
               <li><a href="index.php">Home</a></li>
               <li><a href="about.php">About Us</a></li>
               <li class="active"><a href="#">Art Works</a></li>
               <li><a href="artists.php">Artists</a></li>
               <li class="dropdown">
                 <a data-toggle="dropdown" class="dropdown-toggle" href="#">Specials <b class="caret"></b></a>
                 <ul class="dropdown-menu">
                   <li><a href="#">Special 1</a></li>
                   <li><a href="#">Special 2</a></li>                   
                 </ul>
               </li>
             </ul>              
            </div>
         </nav>  <!-- end .navbar --> 
      </div>  <!-- end container -->
   </div>  <!-- end mainNavigationRow -->
   
</header>
  
  
  
  
  
  <!-- //////////////////////////////////////////////////////////////////////      -->
  
  
  
  
  
  
  
  
  
      
  <div class="container">
   <div class="row">
<!-- //////////////////////////////////////////////////////////////////////      -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">

    <div class="carousel-inner">
        <?php
            $Works = new ArtCollection();
            $firstIteration = 0;
            foreach($Works->collection as $painting){
                if($firstIteration == 0){
                    $firstIteration++;
                    $painting->printArtWork(1);
                }
                else
                    $painting->printArtWork(0);
            }
        ?>
    </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>
             </div>
            </div>
   <!-- //////////////////////////////////////////////////////////////////////      -->
          <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="Resources/bootstrap/js/bootstrap.min.js"></script>
    





    </body>
</html>