<?php
include "ArtWork.php";
include "ArtCollection.php";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
   
   <meta http-equiv="Content-Type" content="text/html;charset=us-ansi">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Lab3 SE3316A</title>
    
<link href='https://fonts.googleapis.com/css?family=Cuprum|Cookie' rel='stylesheet' type='text/css'>  

    <!-- Bootstrap core CSS -->
    <link href="Resources/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="carousel.css" rel="stylesheet">
  </head>
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">LAB 3</a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="work.php">Work</a></li>  
                <li><a href="artists.php">Artists</a></li>                
              </ul>
            </div>
          </div>
        </div>

      </div>
    </div>
    
    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
      </ol>
    <div class="carousel-inner">
      <?php
        $jumboPics = new ArtCollection();
        for($i = 0; $i < 5; $i++){
          if($i == 0)
            $jumboPics->collection[$i]->printArtJumbotron(1);
          else
            $jumboPics->collection[$i]->printArtJumbotron(0);
        }
      ?>
    </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>
    

    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <?php
          $row1Pics = new ArtCollection();
          for($i = 0; $i < 3; $i++){
            $row1Pics->collection[$i + 5]->printRowPic();
          }
        ?>
      </div><!-- /.row -->

      <div class="row">
        <?php
          $row2Pics = new ArtCollection();
          for($i = 0; $i < 3; $i++){
            $row2Pics->collection[$i + 8]->printRowPic();
          }
        ?>
      </div><!-- /.row -->

    </div><!-- /.container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="Resources/bootstrap/js/bootstrap.min.js"></script>
    
      </body>
</html>